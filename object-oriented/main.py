import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import helper as hplot
from CNN import CNN
from Object import Object

DEBUG = True
total_iterations = 0
train_batch_size = 64

def load_data():
    # Carregando dados
    return input_data.read_data_sets('data/MNIST',one_hot= True)

def debug(label,data):
    if DEBUG == True:
        print("Label: {0}, \t \t Data: {1}".format(label,data))


def main ():
    cnn = Object()
    cnn.filter_size1 = 5 # Filtros convolucional de 5 x 5 pixels
    cnn.num_filters1 = 16  # 16 desses Filtros
    cnn.filter_size2 = 5 # Filtros convolucional de 5 x 5 pixels
    cnn.num_filters2 = 36  # 36 desses Filtros
    cnn.layer_size = 128 # Numero de neuronios por camada
    cnn.data = load_data()
    cnn.img_size = 28 # tamanho de pixel de cada dimensao
    cnn.num_channel = 1 # Quantidade de canais
    cnn.num_classes = 10 # Uma classe para cada digito
    Conv = CNN(cnn)
    obj = Conv.create_cnn()

    session = tf.Session()
    session.run(tf.global_variables_initializer())

    # Testes Realizados
    Conv.print_test_accuracy(obj, session,True, True)
    # coloca numero de iteracoes1
    Conv.optimize(session, obj, num = 1000)
    # acuracia apos otimizacao
    Conv.print_test_accuracy(obj, session, True, True)
    # print imagem
    #image1 = cnn.data.test.images[0]
    #hplot.plot_image(image1,((cnn.img_size,cnn.img_size)))
    # print conv
    #hplot.plot_conv_weights(session, weights= obj.weights_conv1)
    # print conv layer
    #hplot.plot_conv_layer(layer=obj.layer_conv1, image=image1, x = obj.x, session = session)

if __name__ == '__main__':
   main()
