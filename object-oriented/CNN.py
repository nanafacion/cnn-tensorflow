import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np
from sklearn.metrics import confusion_matrix
import time
from datetime import timedelta
import math
import helper as hplot
from Object import Object
#
# Autora: Nathana Facion
# Objetivo: Criar uma cnn simples
# Baseado em :https://github.com/Hvass-Labs/TensorFlow-Tutorials/blob/master/02_Convolutional_Neural_Network.ipynb

class CNN(object):

    def __init__(self,cnn):
        # convolutional layer 1
        self.filter_size1 = cnn.filter_size1 # Filtros convolucional de 5 x 5 pixels
        self.num_filters1 = cnn.num_filters1  # 16 desses Filtros
        # convolutional layer 2
        self.filter_size2 = cnn.filter_size2 # Filtros convolucional de 5 x 5 pixels
        self.num_filters2 = cnn.num_filters2  # 36 desses Filtros
        # tamanho da camada
        self.layer_size = cnn.layer_size # Numero de neuronios por camada
        self.data = cnn.data
        self.img_size = cnn.img_size # tamanho de pixel de cada dimensao
        self.img_size_flat = cnn.img_size * cnn.img_size # Um vetor de uma unica dimensao
        self.img_tuple = (cnn.img_size,cnn.img_size) # Uma tupla usada para reshape
        self.num_channel = cnn.num_channel # Quantidade de canais
        self.num_classes = cnn.num_classes # Uma classe para cada digito
        # Seleciona primeiras imagens de teste
        self.images = cnn.data.test.images[0:9]
        # Seleciona classes corretas
        self.correct_class =cnn.data.test.labels[0:9]
        # max valor da label
        self.data.test.cls = np.argmax(cnn.data.test.labels, axis=1)
        # tomar cuidado tamanho da RAM / pode estourar
        self.train_batch_size = 64
        self.test_batch_size = 256
        # inicia valor atualizado na entropia e acuracia
        self.optimizer = 0
        self.accuracy = 0
        # usado para otimizar
        self.total_iterations = 0
        #hplot.plot_images(images = self.images, cls_true = self.correct_class, img_shape = self.img_tuple)


    def new_weights(self, shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

    def new_biases(self, length):
        return tf.Variable(tf.constant(0.05, shape=[length]))

    def new_conv( self, input, num_channel, filter_size, num_filters, use_pooling = True):
        # layer : layer anterior
        # num_channel : o numero de canais da camada anterior
        # num_filter : numero de Filtro
        # use_pooling : usa 2 x 2

        # Forma de peso de filtro para convolucao
        # Este eh determinado pelo Tensorflow
        shape = [filter_size, filter_size, num_channel, num_filters]

        # Cria um novo peso
        w = self.new_weights(shape = shape)

        # Cria um novo baise
        b = self.new_biases(length = num_filters)

        # Criando convolucao
        # stride 1 move 1 para todas as dimensoes
        # padding eh setado para `same` com media da imagem de input
        layer = tf.nn.conv2d(input=input, filter= w,strides=[1,1,1,1], padding='SAME')

        layer = layer + b

        if use_pooling:
            # considera janela 2x2 e seleciona o maior valor de cada janela
            # entao movelos 2 pixel para a proxima janela
            layer = tf.nn.max_pool(value=layer,
                                   ksize=[1, 2, 2, 1],
                                   strides=[1, 2, 2, 1],
                                   padding='SAME')

        # Relu usado para aprender redes neurais mais complicadas
        layer = tf.nn.relu(layer)
        return layer, w

    def flatten_layer(self,layer):
        layer_shape = layer.get_shape()
        # num_elements : img_height * img_width * num_channels
        num_features = layer_shape[1:4].num_elements()

        # Reshape :[num_images, num_features].
        # esta -1 pois o reshape vai calcular o numero de imagens
        layer_flat = tf.reshape(layer, [-1, num_features])
        return layer_flat, num_features

    def new_fc_layer(self,input, num_inputs, num_outputs,  use_relu=True):
        # his function creates a new fully-connected layer in the computational graph for TensorFlow.
        # input: Camada anterior
        # num_inputs : Numero de inputs da camada anterior
        # use_relu : Usar Rectified Linear Unit (ReLU)

        # Calcula novos pesos e baies
        weights = self.new_weights(shape=[num_inputs, num_outputs])
        biases =self.new_biases(length=num_outputs)
        layer = tf.matmul(input, weights) + biases

        if use_relu:
            layer = tf.nn.relu(layer)

        return layer

    def create_cnn(self):
        o = Object()
        # placeholder eh o input no grafo
        o.x = tf.placeholder(tf.float32, shape = [None, self.img_size_flat], name = 'x')
        # num_images pode ser inferido automaticamente usando -1
        o.x_image = tf.reshape(o.x, [-1, self.img_size, self.img_size, self.num_channel])
        o.y_true = tf.placeholder(tf.float32, shape = [None, self.num_classes], name = 'y_true')
        # Nos podemos ter um placeholder variable para o numero de classes,
        # Mas isso eh calculado pelo argmax
        o.y_true_cls = tf.argmax(o.y_true, dimension=1)

        # Aplica a primeira convolucao
        o.layer_conv1, o.weights_conv1 = self.new_conv(input = o.x_image, num_channel = self.num_channel, filter_size = self.filter_size1, num_filters = self.num_filters1, use_pooling = True)
        # Aplica a segunda convolucao
        o.layer_conv2, o.weights_conv2 = self.new_conv(input = o.layer_conv1, num_channel = self.num_filters1, filter_size = self.filter_size1, num_filters = self.num_filters1, use_pooling = True)
        #As camadas convolucionais produzem tensorflow 4-dim. Agora desejamos usa-los como entrada em uma rede totalmente conectada,
        # o que requer que os tensorflow sejam remodelados ou achatados para tensorflow 2-dim.
        o.layer_flat, o.num_features = self.flatten_layer(o.layer_conv2)
        # Adicione uma camada totalmente conectada a rede. A entrada eh a camada achatada da convolucao anterior.
        # O numero de neuronios ou nos na camada totalmente conectada eh layer_size. ReLU eh usado para que possamos aprender relacoes nao-lineares.
        o.layer_fc1 = self.new_fc_layer(input = o.layer_flat,num_inputs = o.num_features, num_outputs = self.layer_size, use_relu = True)
        # Adicione outra camada totalmente conectada que execute vetores de comprimento 10 para determinar quais das 10 classes a qual a imagem de
        # entrada pertence. Observe que o ReLU nao eh usado nesta camada.
        o.layer_fc2 = self.new_fc_layer(input = o.layer_fc1,num_inputs = self.layer_size,num_outputs = self.num_classes, use_relu = False)
        # normalizando o valor de saida para 0 e 1
        o.y_pred = tf.nn.softmax(o.layer_fc2)
        # pega o maior elemento
        o.y_pred_cls = tf.argmax(o.y_pred, dimension = 1)

        self.entropy_cnn(o.layer_fc2, o.y_true)
        self.accuracy_cnn(o.y_pred_cls, o.y_true_cls)
        return o

    def entropy_cnn(self, layer_fc2, y_true):
        # A entropia cruzada eh uma medida de desempenho usada na classificacao. A entropia cruzada eh uma funcao
        # continua que eh sempre positiva e, se a saida prevista do modelo corresponde exatamente
        # a saida desejada, a entropia cruzada eh igual a zero. O objetivo da otimizacao eh, portanto, minimizar a
        # entropia cruzada para que ele fique tao proximo do zero quanto possivel alterando as
        # variaveis das camadas da rede.
        # TensorFlow possui uma funcao integrada para calcular a entropia cruzada. Observe que a funcao calcula o
        # softmax internamente, entao devemos usar a saida de layer_fc2 diretamente em vez
        # de y_pred, que jah teve o softmax aplicado.
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits = layer_fc2, labels = y_true)
        # media da entropia cruzada da imagem toda
        cost = tf.reduce_mean(cross_entropy)
        # usado AdamOptimizer que eh uma forma de gradiente descendente
        # a otimizacao nao eh realizada agora, apenas eh adicionado o objeto otimizador
        # ao grafico TensorFlow para execucao posterior
        self.optimizer = tf.train.AdamOptimizer(learning_rate = 1e-4).minimize(cost)
        # prever classe corretas

    def accuracy_cnn(self,y_pred_cls, y_true_cls):
        correct_prediction = tf.equal(y_pred_cls, y_true_cls)
        # realizar acuracia
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))


    def print_test_accuracy(self, obj, session, show_example_errors = False, show_confusion_matrix = False):
        # obj contem dados necessarios : x, y_pred_cls, y_true
        # imprime a precisao da classificacao no conjunto de testes
        num_test = len(self.data.test.images)
        # aloca matriz para ser preenchida
        cls_pred = np.zeros(shape = num_test, dtype = np.int)
        i = 0
        while i < num_test:
            # considere j o inidice final
            j = min(i + self.test_batch_size, num_test)
            images = self.data.test.images[i:j, :]
            labels = self.data.test.labels[i:j, :]
            feed_dict = {obj.x: images, obj.y_true: labels}
            # predicao da classe com tensorflow
            cls_pred[i:j] = session.run(obj.y_pred_cls, feed_dict=feed_dict)
            i = j

        # numeros de classe verdadeiros do conjunto de teste.
        cls_true = self.data.test.cls
        correct = (cls_true == cls_pred)
        correct_sum = correct.sum()
        acc = float(correct_sum)/num_test

        msg = "Acuracia do conjunto de teste {0:1%} ({1}/{2})"
        print(msg.format(acc, correct_sum, num_test))
        if show_example_errors:
            print("Erros:")
            hplot.plot_example_errors(cls_pred = cls_pred, correct = correct, img_shape = self.img_tuple, data = self.data)
        if show_confusion_matrix:
            print("Matrix confusao:")
            hplot.plot_confusion_matrix(cls_pred = cls_pred, data = self.data, num_classes = self.num_classes)

    def optimize(self,session, obj, num):
        # Funcao para executar uma serie de iteracoes de otimizacao de modo a melhorar gradualmente
        # as variaveis das camadas da rede Em cada iteracao, um novo
        # lote de dados eh selecionado a partir do conjunto de treinamento e, em seguida,
        # TensorFlow executa o otimizador usando essas amostras de treinamento.
        global total_iterations

        start_time = time.time()
        for i in range(self.total_iterations,self.total_iterations + num):

            x_batch, y_true_batch = self.data.train.next_batch(self.train_batch_size)

            feed_dict_train = {obj.x:x_batch, obj.y_true: y_true_batch}

            session.run(self.optimizer, feed_dict= feed_dict_train)

            # Print status das 100 iteracoes
            if i % 100 == 0:
                # Calcula a acuracia do treinamento
                acc = session.run(self.accuracy, feed_dict = feed_dict_train)
                # Mensagem
                msg = "Optimization Iteration {0:>6}, Training Accuracy: {1:>6.1%}"
                print(msg.format(i+1,acc))

        self.total_iterations = self.total_iterations + num
        end_time = time.time()
        time_dif = end_time - start_time
        print("Tempo usado: "+ str(timedelta(seconds=int(round(time_dif)))))
