import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np
from sklearn.metrics import confusion_matrix
import time
from datetime import timedelta
import math
import helper as hplot

#
# Autora: Nathana Facion
# Objetivo: Criar uma cnn simples
# Baseado em :https://github.com/Hvass-Labs/TensorFlow-Tutorials/blob/master/02_Convolutional_Neural_Network.ipynb

DEBUG = True
total_iterations = 0
train_batch_size = 64

def load_data():
    # Carregando dados
    return input_data.read_data_sets('data/MNIST',one_hot= True)

def debug(label,data):
    if DEBUG == True:
        print("Label: {0}, \t \t Data: {1}".format(label,data))

def new_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

def new_biases(length):
    return tf.Variable(tf.constant(0.05, shape=[length]))

def new_conv(input, num_channel, filter_size, num_filters, use_pooling = True):
    # layer : layer anterior
    # num_channel : o numero de canais da camada anterior
    # num_filter : numero de Filtro
    # use_pooling : usa 2 x 2

    # Forma de peso de filtro para convolucao
    # Este eh determinado pelo Tensorflow
    shape = [filter_size, filter_size, num_channel, num_filters]

    # Cria um novo peso
    w = new_weights(shape = shape)

    # Cria um novo baise
    b = new_biases(length = num_filters)

    # Criando convolucao
    # stride 1 move 1 para todas as dimensoes
    # padding eh setado para `same` com media da imagem de input
    layer = tf.nn.conv2d(input=input, filter= w,strides=[1,1,1,1], padding='SAME')

    layer = layer + b

    if use_pooling:
        # considera janela 2x2 e seleciona o maior valor de cada janela
        # entao movelos 2 pixel para a proxima janela
        layer = tf.nn.max_pool(value=layer,
                               ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1],
                               padding='SAME')

    # Relu usado para aprender redes neurais mais complicadas
    layer = tf.nn.relu(layer)
    return layer, w

def flatten_layer(layer):
    layer_shape = layer.get_shape()
    # num_elements : img_height * img_width * num_channels
    num_features = layer_shape[1:4].num_elements()

    # Reshape :[num_images, num_features].
    # esta -1 pois o reshape vai calcular o numero de imagens
    layer_flat = tf.reshape(layer, [-1, num_features])
    return layer_flat, num_features

def new_fc_layer(input, num_inputs, num_outputs,  use_relu=True):
    # his function creates a new fully-connected layer in the computational graph for TensorFlow.
    # input: Camada anterior
    # num_inputs : Numero de inputs da camada anterior
    # use_relu : Usar Rectified Linear Unit (ReLU)

    # Calcula novos pesos e baies
    weights = new_weights(shape=[num_inputs, num_outputs])
    biases = new_biases(length=num_outputs)
    layer = tf.matmul(input, weights) + biases

    if use_relu:
        layer = tf.nn.relu(layer)

    return layer

def main ():
    # convolutional layer 1
    filter_size1 = 5 # Filtros convolucional de 5 x 5 pixels
    num_filters1 = 16  # 16 desses Filtros

    # convolutional layer 2
    filter_size2 = 5 # Filtros convolucional de 5 x 5 pixels
    num_filters2 = 36  # 36 desses Filtros

    # tamanho da camada
    layer_size = 128 # Numero de neuronios por camada

    data = load_data()
    debug('Data',data)

    img_size = 28 # tamanho de pixel de cada dimensao
    img_size_flat = img_size * img_size # Um vetor de uma unica dimensao
    img_tuple = (img_size,img_size) # Uma tupla usada para reshape
    num_channel = 1 # Quantidade de canais
    num_classes = 10 # Uma classe para cada digito

    # Seleciona primeiras imagens de teste
    images = data.test.images[0:9]
    # Seleciona classes corretas
    correct_class =data.test.labels[0:9]

    # max valor da label
    data.test.cls = np.argmax(data.test.labels, axis=1)

    # plota grafico das imagens
    hplot.plot_images(images = images, cls_true = correct_class, img_shape = img_tuple)


    # placeholder eh o input no grafo
    x = tf.placeholder(tf.float32, shape = [None, img_size_flat], name = 'x')
    # num_images pode ser inferido automaticamente usando -1
    x_image = tf.reshape(x, [-1, img_size, img_size, num_channel])
    y_true = tf.placeholder(tf.float32, shape = [None, num_classes], name = 'y_true')
    # Nos podemos ter um placeholder variable para o numero de classes,
    # Mas isso eh calculado pelo argmax
    y_true_cls = tf.argmax(y_true, dimension=1)

    # Aplica a primeira convolucao
    layer_conv1, weights_conv1 = new_conv(input = x_image, num_channel = num_channel, filter_size = filter_size1, num_filters = num_filters1, use_pooling = True)
    # Aplica a segunda convolucao
    layer_conv2, weights_conv2 = new_conv(input = layer_conv1, num_channel = num_filters1, filter_size = filter_size1, num_filters = num_filters1, use_pooling = True)
    #As camadas convolucionais produzem tensorflow 4-dim. Agora desejamos usa-los como entrada em uma rede totalmente conectada,
    # o que requer que os tensorflow sejam remodelados ou achatados para tensorflow 2-dim.
    layer_flat, num_features = flatten_layer(layer_conv2)
    # Adicione uma camada totalmente conectada a rede. A entrada eh a camada achatada da convolucao anterior.
    # O numero de neuronios ou nos na camada totalmente conectada eh layer_size. ReLU eh usado para que possamos aprender relacoes nao-lineares.
    layer_fc1 = new_fc_layer(input = layer_flat,num_inputs = num_features, num_outputs = layer_size, use_relu = True)
    # Adicione outra camada totalmente conectada que execute vetores de comprimento 10 para determinar quais das 10 classes a qual a imagem de
    # entrada pertence. Observe que o ReLU nao eh usado nesta camada.
    layer_fc2 = new_fc_layer(input = layer_fc1,num_inputs = layer_size,num_outputs = num_classes, use_relu = False)
    # normalizando o valor de saida para 0 e 1
    y_pred = tf.nn.softmax(layer_fc2)
    # pega o maior elemento
    y_pred_cls = tf.argmax(y_pred, dimension = 1)
    # A entropia cruzada eh uma medida de desempenho usada na classificacao. A entropia cruzada eh uma funcao
    # continua que eh sempre positiva e, se a saida prevista do modelo corresponde exatamente
    # a saida desejada, a entropia cruzada eh igual a zero. O objetivo da otimizacao eh, portanto, minimizar a
    # entropia cruzada para que ele fique tao proximo do zero quanto possivel alterando as
    # variaveis das camadas da rede.
    # TensorFlow possui uma funcao integrada para calcular a entropia cruzada. Observe que a funcao calcula o
    # softmax internamente, entao devemos usar a saida de layer_fc2 diretamente em vez
    # de y_pred, que jah teve o softmax aplicado.
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits = layer_fc2, labels = y_true)
    # media da entropia cruzada da imagem toda
    cost = tf.reduce_mean(cross_entropy)
    # usado AdamOptimizer que eh uma forma de gradiente descendente
    # a otimizacao nao eh realizada agora, apenas eh adicionado o objeto otimizador
    # ao grafico TensorFlow para execucao posterior
    optimizer = tf.train.AdamOptimizer(learning_rate = 1e-4).minimize(cost)
    # prever classe corretas
    correct_prediction = tf.equal(y_pred_cls, y_true_cls)
    debug('Predicao correta', correct_prediction)
    # realizar acuracia
    accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
    debug('Acuracia', accuracy)

        # inicia a sessao do tensorflow
    session = tf.Session()
    session.run(tf.global_variables_initializer())

    # tomar cuidado tamanho da RAM / pode estourar
    test_batch_size = 256

    def print_test_accuracy(show_example_errors = False, show_confusion_matrix = False):
        # imprime a precisao da classificacao no conjunto de testes
        num_test = len(data.test.images)
        # aloca matriz para ser preenchida
        cls_pred = np.zeros(shape = num_test, dtype = np.int)
        i = 0
        while i < num_test:
            # considere j o inidice final
            j = min(i + test_batch_size, num_test)
            images = data.test.images[i:j, :]
            labels = data.test.labels[i:j, :]
            feed_dict = {x: images, y_true: labels}
            # predicao da classe com tensorflow
            cls_pred[i:j] = session.run(y_pred_cls, feed_dict=feed_dict)
            i = j

        # numeros de classe verdadeiros do conjunto de teste.
        cls_true = data.test.cls
        correct = (cls_true == cls_pred)
        correct_sum = correct.sum()
        acc = float(correct_sum)/num_test

        msg = "Acuracia do conjunto de teste {0:1%} ({1}/{2})"
        print(msg.format(acc, correct_sum, num_test))

        if show_example_errors:
            print("Erros:")
            hplot.plot_example_errors(cls_pred = cls_pred, correct = correct, img_shape = img_tuple, data = data)
        if show_confusion_matrix:
            print("Matrix confusao:")
            hplot.plot_confusion_matrix(cls_pred = cls_pred, data = data, num_classes = num_classes)

    def optimize(num):
        # Funcao para executar uma serie de iteracoes de otimizacao de modo a melhorar gradualmente
        # as variaveis das camadas da rede Em cada iteracao, um novo
        # lote de dados eh selecionado a partir do conjunto de treinamento e, em seguida,
        # TensorFlow executa o otimizador usando essas amostras de treinamento.
        global total_iterations

        start_time = time.time()
        for i in range(total_iterations,total_iterations + num):

            x_batch, y_true_batch = data.train.next_batch(train_batch_size)

            feed_dict_train = {x:x_batch, y_true: y_true_batch}

            session.run(optimizer, feed_dict= feed_dict_train)

            # Print status das 100 iteracoes
            if i % 100 == 0:
                # Calcula a acuracia do treinamento
                acc = session.run(accuracy, feed_dict = feed_dict_train)
                # Mensagem
                msg = "Optimization Iteration {0:>6}, Training Accuracy: {1:>6.1%}"
                print(msg.format(i+1,acc))

        total_iterations = total_iterations + num
        end_time = time.time()
        time_dif = end_time - start_time
        print("Tempo usado: "+ str(timedelta(seconds=int(round(time_dif)))))

    # Realizacao de testes
    # acuracia sem otimizacao
    print_test_accuracy(True,True)
    # coloca numero de iteracoes
    optimize(num = 1000)
    # acuracia apos otimizacao
    print_test_accuracy(True,True)
    # print imagem
    image1 = data.test.images[0]
    hplot.plot_image(image1,img_tuple)
    # print conv
    hplot.plot_conv_weights(session, weights=weights_conv1)
    # print conv layer
    hplot.plot_conv_layer(layer=layer_conv1, image=image1, x = x, session = session)

if __name__ == '__main__':
   main()
