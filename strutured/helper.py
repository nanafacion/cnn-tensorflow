import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np
from sklearn.metrics import confusion_matrix
import time
from datetime import timedelta
import math


def plot_images(images, cls_true, img_shape, cls_pred=None):
    # o grid eve ter 9 imagens
    assert len(images) == len(cls_true) == 9

    # criamos um grid 3 x 3 pois teremos 9 imagens
    fig, axes = plt.subplots(3,3)
    fig.subplots_adjust(hspace = 0.3,wspace = 0.3)

    for i,ax in enumerate(axes.flat):
        ax.imshow(images[i].reshape(img_shape), cmap = 'binary')
        if cls_pred is None:
            xlabel = "Real: {0}".format(cls_true[i])
        else:
            xlabel = "Real: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])
        ax.set_xlabel(xlabel)

        ax.set_xticks([])
        ax.set_yticks([])

    plt.show()

def plot_example_errors(cls_pred, correct, img_shape, data):
    # cls_pred eh a matriz de classe prevista pata todo conjunto de teste
    # correct eh uma matriz booleana de classe prevista
    incorrect = (correct == False)
    # seleciona imagens incorretas
    images = data.test.images[incorrect]
    # seleciona a classe prevista para a imagem
    cls_pred = cls_pred[incorrect]
    cls_true = data.test.cls[incorrect]
    plot_images(images = images[0:9],cls_true = cls_true[0:9], img_shape = img_shape, cls_pred = cls_pred[0:9])


def plot_confusion_matrix(cls_pred, data, num_classes):
    cls_true = data.test.cls
    # matriz por meio de sklearn
    cm = confusion_matrix(y_true = cls_true, y_pred = cls_pred)
    print(cm)
    # plota matriz de confusao
    plt.matshow(cm)
    plt.colorbar()
    tick_marks = np.arange(num_classes)
    plt.xticks(tick_marks, range(num_classes))
    plt.yticks(tick_marks, range(num_classes))
    plt.xlabel('Predicted')
    plt.ylabel('Real')
    plt.show()

def plot_conv_weights(session,weights, input_channel = 0):
    # Recupere os valores das variaveis de peso do tensorflow
    # A feed-dict nao eh necessario porque nada eh calculado.
    w = session.run(weights)
    # Isso eh usado para corrigir a intensidade da cor em
    # as imagens para que elas possam ser comparadas entre si.
    w_min = np.min(w)
    w_max = np.max(w)
    num_filters = w.shape[3]
    # numero de grid para plot
    num_grids = int(math.ceil(math.sqrt(num_filters)))
    # cria uma imagem com um grid de sub-plots
    fig, axes = plt.subplots(num_grids, num_grids)

    # plot todos os pesos do filtro
    for i, ax in enumerate(axes.flat):
        if i < num_filters:
            img = w[:,:, input_channel,i]
            ax.imshow(img, vmin = w_min, vmax = w_max, interpolation = 'nearest', cmap = 'seismic')

        ax.set_xticks([])
        ax.set_yticks([])

    plt.show()

def plot_conv_layer(layer, image, x, session):
    # Crie um feed-dict contendo apenas uma imagem.
    feed_dict = {x: [image] }
    # Calcular e recuperar os valores de saida da camada ao inserir essa imagem.
    # Observe que nao precisamos alimentar o y_true porque eh
    # Nao utilizado neste calculo
    values = session.run(layer, feed_dict = feed_dict)
    num_filters = values.shape[3]
    num_grids = int(math.ceil(math.sqrt(num_filters)))
    fig,axes = plt.subplots(num_grids, num_grids)
    for i,ax in enumerate(axes.flat):
        if i < num_filters:
            img = values[0,:,:,i]
            ax.imshow(img,interpolation = 'nearest', cmap = 'binary')
        ax.set_xticks([])
        ax.set_yticks([])
    plt.show()

def plot_image(image, img_shape):
    plt.imshow(image.reshape(img_shape), interpolation = 'nearest', cmap = 'binary')
    plt.show()
