
# CNN com Tensorflow

Este projeto tem como objetivo a implementação de uma CNN com Tensorflow.
Na pasta object-oriented encontramos uma versão orientada objeto e na
pasta strutured uma versão estruturada.

## Funcionamento da rede

### O que é Convolução ?
* Uma operação
* Todos efeitos podem ser pensados como filtros, ou se pensarmos em machine learning de kernels.
* A diferença entre convolução e correção é apenas um sinal

### Como ela pode ser implementada ?
```
def convolve(x,w):
	y = np.zeros(x.shape)
	for n in xrange(x.shape[0]):
		for m in xrange(x.shape[1]):
			for i in xrange(w.shape[0]):
				for j in xrange(w.shape[1]):
					y[n,m] += x[n-i, m-j] * w[i,j]
```

### Importantes:
* Depois da convolução nÓS sabemos se uma feature
foi encontrada, então pegamos os vizinhos(square) dos dados e
conseguimos o max ( chamado maxpolling).
* Falamos 4D porque temos : N x channel x width x height.
* Pooling é outra palavra usada para downsampling.
* Se vc tem a poolsize of 2x2, então a cada 4 é escolhido 1
* Maxpooling : pega o max entre 4 pixel.
* Average Pooling: pega a media de 4 pixel.

## Instalação para rodar CNN #

* pip:
```
https://pip.pypa.io/en/stable/installing/
```

* Tensorflow:
Link para instalação do Tensorflow:
```
https://www.tensorflow.org/install/install_linux#nvidia_requirements_to_run_tensorflow_with_gpu_support
```

* Para o funcionamento é necessário setar as variaveis de ambiente que estao nesse link (Lembrando que tem que olhar a versão do cuDNN que esta sendo usada):
```
https://stackoverflow.com/questions/41991101/importerror-libcudnn-when-running-a-tensorflow-program
```

* Matplotlib e outros pacotes relevantes:
```
python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
```

* Dependencia necessária para o Matplotlib:
```
apt-get install  python-tk
```

* Sklearn:
```
pip install -U scikit-learn
```

## Executar #
* Acesse a pasta **object-oriented**  e no terminal digite:
``` python main.python ```
* Acesse a pasta **strutured** e no terminal digite:
``` python cnn.python ```

## Video que auxilia no ensinamento de CNN:
```
https://www.youtube.com/watch?v=DXnyuUZcAAI
```

### Importante anotações sobre o video:

* Temos uma função e um kernel.

* Multiplicamos eles.(Convolução)

* Selecionamos os maiores (Perceba que ele detecta padrão do kernel, os lugares
que são similar ao filtro será maior)

* Imagem resultante é menor.(Ignora a borda, assuma que a borda tem zero, replica o último número)

* Na arquitetura você pode escolher se o passo na função para multiplicar o filtro vai ser de 1, de 2. Existem várias formas.

* Então nós temos que a nossa entrada é a função (neuronio), os nossos pesos
são os filtros(aresta/peso) e a nossa saída(neuronio)  é o resultado da multiplicação.
Também pode ser chamada de mapa de atributo.

* Na rede convolucionais voce aprende esses pesos por meio do backpropagation.
O backpropagation nada mais é que um tipo de decida do gradiente.

* Esse processo de multiplicação que é realizado pode ser visto como produto 
escalar da algebra linear e é a forma que vemos o grau de similaridade
entre os vetores.

* A Convolução obriga que os pesos se repitam (lembre que o peso é o filtro).
Isso é importante porque você diminui o número de parâmetros que você precisa 
aprender.

* Usa-se função max(0,x)/softmax na saída porque isso melhora o processamento
já que as multiplicações vão dar 0. Todo número negativo vira zero.
Outra técnica é usar o max-pooling pois pega a cada determinada região
com isso pequenas variações não influenciam no resultado

* Cada camada da convolução detecta algo diferente. A cada camada então temos caracteristicas
diferentes que pode fazer com que tenhamos uma hierarquia para identificar cada uma delas.

* Essa parte da rede convlucional que envolve: convolução e subsampling
é chamada na visão computacional de extração de atributos.
A parte chamada de fully connected já é a parte de classificação.


